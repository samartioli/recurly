$(function() {


	$('#jsonOne').click(function() {
		$.getJSON('json/one.json', function(data) {
			processReturn(data);
		});
	});

	$('#jsonTwo').click(function() {
		$.getJSON('json/two.json', function(data) {
			processReturn(data);
		});
	});

	$('#jsonThree').click(function() {
		$.getJSON('json/three.json', function(data) {
			processReturn(data);
		});
	});

	$("#input").change(function() {
	    $.getJSON(window.URL.createObjectURL($('#input').get(0).files[0]), function(data) {
	    	processReturn(data);
	    });
	});


});

var processReturn = function(data) {

	var cc_array
		, cc_rev
		, sum_a = 0
		, array_b = []
		, array_c = []
		, sum_b = 0
		, i
		, j
		, cardType
		, toReturn = [];
	
		//console.log(data);

	if (data.length == 0) { write('### INFO: Empty Array') } else {

		for (i = 0; i < data.length; i++) {
			cc_array = data[i].split('');
			cardType = getCardType(cc_array[0]);
			//reverse digits
			cc_rev = cc_array.reverse();

			//sum odd digits
			for (j=0; j<cc_rev.length; j+=2) {
				sum_a += parseInt(cc_rev[j]);
			}

			//multiply even digits by 2
			for (j=1; j<cc_rev.length; j+=2) {
				array_b.push((cc_rev[j]*2).toString());
			}

			//extrapolate double digits from above and sum
			for (j=0; j<array_b.length; j++) {
				array_c = array_b[j].split('');
				for (k=0; k<array_c.length; k++) {
					sum_b += parseInt(array_c[k]);
				}
			}

			toReturn.push({
				"valid" : Boolean(!((sum_a + sum_b)%10)),
				"type" : cardType
			});

			// console.log(sum_a + sum_b);
			// console.log(cardType);
			// console.log(toReturn);

			sum_a = sum_b = 0;
			array_b = array_c = [];
			cardType = '';

		}

		write(JSON.stringify(toReturn));

	}

};

var getCardType = function(firstChar) {

	switch(firstChar) {
		case '3' : return 'amex';
		case '4' : return 'visa';
		case '5' : return 'mastercard';
		case '6' : return 'discover';
		default : return 'INVALID'

	}

}

var write = function(str) {

	console.log(str)
	$('#output').html(str);

};